import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { User, Users } from './core/ngrx/models/user.model';
@Injectable({
  providedIn: 'root',
})
export class UsersListService {
  getUsers(page: number, per_page: number): Observable<Users> {
    const start = page * per_page;
    const end = start + per_page;
    let data: Users = {
      page: page,
      per_page: per_page,
      total: UsersList.length,
      data: UsersList.slice(start, end),
    };
    return of(data);
  }
  addUser(user: any): Observable<User> {
    let addedUser: User = {
      id: UsersList.length + 1,
      firstName: user.firstName,
      lastName: user.lastName,
      birthdate: user.birthdate,
      isActive: true,
    };
    UsersList.unshift(addedUser);
    return of(addedUser);
  }
  getUserDetails(id: number): Observable<User> {
    const user = UsersList.find((u) => u.id == id);

    if (!user) {
      throw new Error('User not found');
    }
    return of(user);
  }
  deleteUser(id: number) {
    const index = UsersList.findIndex((u) => u.id == id);
    if (index !== -1) {
      UsersList.splice(index, 1);
      return of({ success: true, message: 'User deleted successfully.' });
    } else {
      return throwError(() => new Error('User not found'));
    }
  }
  updateUser(user: User): Observable<User> {
    console.log(user);

    const index = UsersList.findIndex((u) => u.id == user.id);
    if (index !== -1) {
      UsersList[index] = { ...UsersList[index], ...user };
      return of(UsersList[index]);
    } else {
      return throwError(() => new Error('User not found'));
    }
  }
}
export const UsersList: User[] = [
  {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    birthdate: '1990-01-01',
    isActive: true,
  },
  {
    id: 2,
    firstName: 'Jane',
    lastName: 'Doe',
    birthdate: '1991-02-02',
    isActive: false,
  },
  {
    id: 3,
    firstName: 'Michael',
    lastName: 'Smith',
    birthdate: '1992-03-03',
    isActive: true,
  },
  {
    id: 4,
    firstName: 'Sarah',
    lastName: 'Johnson',
    birthdate: '1993-04-04',
    isActive: false,
  },
  {
    id: 5,
    firstName: 'Robert',
    lastName: 'Williams',
    birthdate: '1994-05-05',
    isActive: true,
  },
  {
    id: 6,
    firstName: 'Jessica',
    lastName: 'Brown',
    birthdate: '1995-06-06',
    isActive: false,
  },
  {
    id: 7,
    firstName: 'David',
    lastName: 'Jones',
    birthdate: '1996-07-07',
    isActive: true,
  },
  {
    id: 8,
    firstName: 'Emily',
    lastName: 'Garcia',
    birthdate: '1997-08-08',
    isActive: false,
  },
  {
    id: 9,
    firstName: 'James',
    lastName: 'Miller',
    birthdate: '1998-09-09',
    isActive: true,
  },
  {
    id: 10,
    firstName: 'Elizabeth',
    lastName: 'Davis',
    birthdate: '1999-10-10',
    isActive: false,
  },
  {
    id: 11,
    firstName: 'William',
    lastName: 'Rodriguez',
    birthdate: '2000-11-11',
    isActive: true,
  },
  {
    id: 12,
    firstName: 'Jennifer',
    lastName: 'Martinez',
    birthdate: '2001-12-12',
    isActive: false,
  },
  {
    id: 13,
    firstName: 'Richard',
    lastName: 'Hernandez',
    birthdate: '2002-01-13',
    isActive: true,
  },
  {
    id: 14,
    firstName: 'Samantha',
    lastName: 'Lopez',
    birthdate: '2003-02-14',
    isActive: false,
  },
  {
    id: 15,
    firstName: 'Charles',
    lastName: 'Gonzalez',
    birthdate: '2004-03-15',
    isActive: true,
  },
  {
    id: 16,
    firstName: 'Laura',
    lastName: 'Wilson',
    birthdate: '2005-04-16',
    isActive: false,
  },
  {
    id: 17,
    firstName: 'Thomas',
    lastName: 'Anderson',
    birthdate: '2006-05-17',
    isActive: true,
  },
  {
    id: 18,
    firstName: 'Katie',
    lastName: 'Thomas',
    birthdate: '2007-06-18',
    isActive: false,
  },
  {
    id: 19,
    firstName: 'Christopher',
    lastName: 'Taylor',
    birthdate: '2008-07-19',
    isActive: true,
  },
  {
    id: 20,
    firstName: 'Nicole',
    lastName: 'Moore',
    birthdate: '2009-08-20',
    isActive: false,
  },
];
