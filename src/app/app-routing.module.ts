import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserslistComponent } from './userslist/userslist.component';
import { UserformComponent } from './userform/userform.component';

const routes: Routes = [
  { path: '', redirectTo: 'users-list', pathMatch: 'full' },
  {
    path: 'users-list',
    component: UserslistComponent,
    data: { name: 'Users List' },
  },
  {
    path: 'user-form',
    component: UserformComponent,
    data: { name: 'User Form' },
  },
  {
    path: 'user-form/:id',
    component: UserformComponent,
    data: { name: 'User Form' },
  },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
