import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersListService } from '../users-list.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { UserDetailsState } from '../core/ngrx/reducers/users.reducer';
import { selectUserDetails } from '../core/ngrx/selectors/users.selectors';
import { loadUserDetails } from '../core/ngrx/actions/users.action';
import { User } from '../core/ngrx/models/user.model';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.scss'],
  providers: [DatePipe],
})
export class UserformComponent implements OnInit {
  userForm: FormGroup;
  editMode: boolean = false;
  constructor(
    private fb: FormBuilder,
    private usersListService: UsersListService,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<UserDetailsState>
  ) {
    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      birthdate: ['', Validators.required],
      isActive: [false],
    });
  }

  ngOnInit(): void {
    if (this.route.snapshot.params['id']) {
      this.editMode = true;
      let id = this.route.snapshot.params['id'];
      this.store.dispatch(loadUserDetails({ userId: id }));
      this.store.pipe(select(selectUserDetails)).subscribe((res: any) => {
        this.userForm.patchValue(res);
      });
    }
  }

  onSubmit() {
    this.userForm.controls['birthdate'].setValue(
      this.datePipe.transform(
        this.userForm.controls['birthdate'].value,
        'yyyy-MM-dd'
      )
    );
    if (this.editMode) {
      let payload: User = {
        id: this.route.snapshot.params['id'],
        firstName: this.userForm.controls['firstName'].value,
        lastName: this.userForm.controls['lastName'].value,
        birthdate: this.userForm.controls['birthdate'].value,
        isActive: this.userForm.controls['isActive'].value,
      };
      this.usersListService.updateUser(payload);
    } else {
      this.usersListService.addUser(this.userForm.value);
    }
    this.router.navigateByUrl('/users-list');
  }
}
