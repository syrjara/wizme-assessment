import { Component, OnInit } from '@angular/core';
import { User } from '../core/ngrx/models/user.model';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { loadUsers } from '../core/ngrx/actions/users.action';
import { PageEvent } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { selectUsers } from '../core/ngrx/selectors/users.selectors';
import { UserState } from '../core/ngrx/reducers/users.reducer';
import { UsersListService } from '../users-list.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.scss'],
})
export class UserslistComponent implements OnInit {
  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'birthdate',
    'isActive',
    'actions',
  ];
  users$!: Observable<any>;
  users: any[] = [];
  element: any;
  totalCount = 0;
  constructor(
    private router: Router,
    private store: Store<UserState>,
    private usersListService: UsersListService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getUsers(0, 5);
    this.store.pipe(select(selectUsers)).subscribe((res: any) => {
      this.users = res.data;
      this.totalCount = res.total;
    });
  }

  handlePageEvent(page: PageEvent): void {
    this.getUsers(page.pageIndex, page.pageSize);
  }
  getUsers(page: number, per_page: number) {
    this.store.dispatch(loadUsers({ page: page, perPage: per_page }));
  }

  addUser(): void {
    this.router.navigateByUrl('/user-form');
  }

  editUser(user: User): void {
    this.router.navigate(['/user-form', user.id]);
  }

  deleteUser(user: User): void {
    this.usersListService.deleteUser(user.id);
    this.snackBar.open('User Deleted Successfuly', '', { duration: 2000 });
    this.getUsers(0, 5);
  }
}
