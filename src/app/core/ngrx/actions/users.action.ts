import { createAction, props } from '@ngrx/store';
import { User, Users } from '../models/user.model';

export const loadUsers = createAction(
  '[User] Load Users',
  props<{ page: number; perPage: number }>()
);
export const loadUsersSuccess = createAction(
  '[User] Load Users Success',
  props<{ users: Users }>()
);
export const loadUserDetails = createAction(
  '[User] Load User Details',
  props<{ userId: number }>()
);
export const loadUserDetailsSuccess = createAction(
  '[User] Load User Details Success',
  props<{ userDetails: User }>()
);
