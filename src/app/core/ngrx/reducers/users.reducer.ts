import { createReducer, on } from '@ngrx/store';
import { User, Users } from '../models/user.model';
import * as UserActions from '../actions/users.action';

export interface UserState {
  users: Users;
}

export const initialState: UserState = {
  users: {
    page: 0,
    per_page: 0,
    total: 0,
    data: [],
  },
};

export const userReducer = createReducer(
  initialState,
  on(UserActions.loadUsers, (state) => {
    return {
      ...state,
    };
  }),
  on(UserActions.loadUsersSuccess, (state, { users }) => ({
    ...state,
    users,
  }))
);

export interface UserDetailsState {
  userDetails: User;
}

export const initialDetailsState: UserDetailsState = {
  userDetails: {
    firstName: '',
    lastName: '',
    birthdate: '',
    id: 0,
    isActive: false,
  },
};

export const userDetailsReducer = createReducer(
  initialDetailsState,
  on(UserActions.loadUserDetails, (state) => {
    return {
      ...state,
    };
  }),
  on(UserActions.loadUserDetailsSuccess, (state, { userDetails }) => ({
    ...state,
    userDetails,
  }))
);
