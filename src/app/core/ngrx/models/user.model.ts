export interface Users {
  page: number;
  per_page: number;
  total: number;
  data: User[];
}
export interface User {
  id: number;
  firstName: string;
  lastName: string;
  birthdate: string;
  isActive: boolean;
}
