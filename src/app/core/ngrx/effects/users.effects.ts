import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { UsersListService } from '../../../users-list.service';
import {
  loadUserDetails,
  loadUserDetailsSuccess,
  loadUsers,
  loadUsersSuccess,
} from '../actions/users.action';

@Injectable()
export class UserEffects {
  loadUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadUsers),
      mergeMap(({ page, perPage }) =>
        this.usersService
          .getUsers(page, perPage)
          .pipe(map((users) => loadUsersSuccess({ users })))
      )
    )
  );
  loadUserDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadUserDetails),
      mergeMap(({ userId }) =>
        this.usersService
          .getUserDetails(userId)
          .pipe(map((userDetails) => loadUserDetailsSuccess({ userDetails })))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private usersService: UsersListService
  ) {}
}
