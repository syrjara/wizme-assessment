import { createSelector, createFeatureSelector } from '@ngrx/store';
import { UserDetailsState, UserState } from '../reducers/users.reducer';

export const selectUserState = createFeatureSelector<UserState>('users');
export const selectUsers = createSelector(
  selectUserState,
  (state: UserState) => state.users
);
export const selectUserDetailsState =
  createFeatureSelector<UserDetailsState>('userDetails');
export const selectUserDetails = createSelector(
  selectUserDetailsState,
  (state: UserDetailsState) => {
    return state.userDetails;
  }
);
